using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Core.Domain.Administration
{
    /// <summary>
    /// Партнер
    /// </summary>
    public class Employee : BaseEntity
    {
        /// <summary>
        /// Имя
        /// </summary>
        [MaxLength(255)]
        [Description("Имя")]
        public string FirstName { get; set; }
        
        /// <summary>
        /// Фамилия
        /// </summary>
        [MaxLength(255)]
        [Description("Фамилия")]
        public string LastName { get; set; }
        
        /// <summary>
        /// Инициалы (ФИ)
        /// </summary>
        [Description("Инициалы (ФИ)")]
        public string FullName => $"{FirstName} {LastName}";
        
        /// <summary>
        /// Email
        /// </summary>
        [MaxLength(320)]
        [Description("Email")]
        public string Email { get; set; }

        /// <summary>
        /// Адрес
        /// </summary>
        [MaxLength(320)]
        [Description("Юр. адрес")]
        public string Address { get; set; }

        /// <summary>
        /// Роль
        /// </summary>
        public virtual Role Role { get; set; }

        /// <summary>
        /// Количество примененных промокодов
        /// </summary>
        [Description("Количество примененных промокодов")]
        public int NumPromoCodeApplied { get; set; }
    }
}
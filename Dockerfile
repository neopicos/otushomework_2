FROM mcr.microsoft.com/dotnet/core/aspnet:3.1

WORKDIR /app

EXPOSE 5000

ENV ASPNETCORE_URLS http://*:5000

COPY ./ ./

RUN dotnet restore "/OtusWork/OtusWork/OtusWork.sln"

RUN dotnet publish "OtusWork/OtusWork.csproj" -c Release  -o /app

CMD dotnet OtusWork.dll
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using Core.Abstractions;
using Core.Domain.Administration;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using OtusWork.Models;
using OtusWork.Models.Requests;

namespace OtusWork.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeesController(IRepository<Employee> employeeRepository,
                                   IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    FullName = x.FullName,
                    Email = x.Email
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync([Required] Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Role = (employee.Role == null) ? null : new RoleItemResponse()
                {
                    Name = employee.Role.Name,
                    Description = employee.Role.Description,
                },
                FullName = employee.FullName,
                NumPromoCodeApplied = employee.NumPromoCodeApplied
            };

            return employeeModel;
        }
        
        /// <summary>
        /// Удалить существующего сотрудника по Id
        /// </summary>
        [HttpDelete("{id:guid}")]
        public async Task<HttpStatusCode> DeleteEmployeeAsync([Required] Guid id)
        {
            var entity = await _employeeRepository.GetByIdAsync(id);

            if (entity == null)
                return HttpStatusCode.NotFound;

            await _employeeRepository.DeleteAsync(entity);
            
            return HttpStatusCode.OK;
        }
        
        /// <summary>
        /// Добавить нового сотрудника
        /// </summary>
        [HttpPost]
        public async Task<HttpStatusCode> CreateEmployeeAsync(EmployeeRequest request)
        {
            if (!ModelState.IsValid)
                return HttpStatusCode.BadRequest;

            var newEmployee = new Employee()
            {
                Id = Guid.NewGuid(),
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
            };

            await _employeeRepository.AddAsync(newEmployee);

            return HttpStatusCode.Created;
        }
        
        /// <summary>
        /// Изменить данные существующего сотрудника
        /// </summary>
        [HttpPut("{id:guid}")]
        public async Task<HttpStatusCode> EditEmployeeAsync([Required]Guid id, EmployeeRequest employeeRequest)
        {
            if (!ModelState.IsValid)
                return HttpStatusCode.BadRequest;
            
            var oldEmployee = await _employeeRepository.GetByIdAsync(id);
            if (oldEmployee != null)
                return HttpStatusCode.NotFound;

            oldEmployee.FirstName = employeeRequest.FirstName;
            oldEmployee.LastName = employeeRequest.LastName;
            oldEmployee.Email = employeeRequest.Email;
            oldEmployee.NumPromoCodeApplied = employeeRequest.NumPromoCodeApplied;
            oldEmployee.Role = await _roleRepository.GetByIdAsync(employeeRequest.RoleId);

            await _employeeRepository.UpdateAsync(oldEmployee);

            return HttpStatusCode.Accepted;
        }
    }
}
﻿using System;

namespace OtusWork.Models.Responses.PromoCodeManagment
{
    public class CustomerShortResponse
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

    }
}
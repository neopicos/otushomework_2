﻿using System;

namespace OtusWork.Models.Responses.PromoCodeManagment
{
    public class PreferenceResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

    }
}
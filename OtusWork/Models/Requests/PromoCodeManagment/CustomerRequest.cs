﻿using System;
using System.Collections.Generic;
using Core.Domain.PromoCodeManagement;

namespace OtusWork.Models.Requests.PromoCodeManagment
{
    public class CustomerRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        public List<Guid> PreferenceIds { get; set; }
    }
}
using System.ComponentModel.DataAnnotations.Schema;
using Core.Domain.Administration;
using Core.Domain.PromoCodeManagement;

namespace DataAccess.Data
{
    public class EfDbInitializer : IDbInitializer
    {
        private readonly AppDbContext _dbContext;

        public EfDbInitializer(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public void InitializeDb()
        {
            _dbContext.Database.EnsureDeleted();
            _dbContext.Database.EnsureCreated();

            var employees = FakeDataFactory.Employees;
            var roles = FakeDataFactory.Roles;
            var preferences = FakeDataFactory.Preferences;
            var customers = FakeDataFactory.Customers;

            foreach (var customer in customers)
            {
                _dbContext.Set<Customer>().Add(customer);
            }
            
            foreach (var employee in employees)
            {
                _dbContext.Set<Employee>().Add(employee);
            }

            _dbContext.SaveChanges();
        }
    }
}